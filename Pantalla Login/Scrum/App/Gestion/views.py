from django.shortcuts import render, render_to_response, redirect
from django.template import Context, loader
from django.http import HttpResponse
# from .models import Proyecto
from django.views import generic
from django.views.generic import ListView, DetailView
from django.db.models import Q
from django.urls import reverse
from django.views.generic.edit import UpdateView
from django.views.decorators.csrf import ensure_csrf_cookie
import json
from .models import Board, Card, Column, Proyecto
from django.utils.decorators import method_decorator

'''class backlogList(ListView):
	model=Proyecto
	template_name='baseMetodo.html'

	def search(self,request):
		query = request.GET.get('q', '')
		if query:
			qset=(Q(codigo__icontains=query)|
				Q(nombre__icontains=query)|
				Q(ProductOwner__username__icontains=query))
			results = Proyecto.objects.filter(qset).distinct()
		else:
			results = []

		return render_to_response("baseMetodo.html",
			{"results": results,"query": query,
			'columns': Column.objects.all(),
        	'cards':Card.objects.all()})
	@method_decorator(ensure_csrf_cookie)
	def dispatch(self,*args,**kwargs):
		return super().dispatch(*args,**kwargs)
'''
'''class backlogDetail(DetailView):
	model=Proyecto
	template_name='backlog_detail.html'

class backlogUpdate(UpdateView):
	model=Proyecto
	template_name='backlog_form.html'
	fields=['codigo','nombre','descripcion','fechaInicio','fechaEntrega',
	'estado']
	def get_success_url(self):
		return reverse('backlogs:list')'''
url = 0


# def search(self,request):
#		query = request.GET.get('q', '')
#		if query:
#			qset=(Q(codigo__icontains=query)|
#				Q(nombre__icontains=query)|
#				Q(ProductOwner__username__icontains=query))
#			results = Proyecto.objects.filter(qset).distinct()
#		else:
#			results = []
#
#		return render_to_response("baseMetodo.html",
#			{"results": results,"query": query})

@ensure_csrf_cookie
def index(request):
    query = request.GET.get('q', '')
    global url
    url = query
    if query:
        qset = (Q(codigo_proyecto__icontains=query) |
                Q(nombre_proyecto__icontains=query) |
                Q(ProductOwner__username__icontains=query))
        results = Proyecto.objects.filter(qset).distinct()
    else:
        results = []
    return render(request, template_name='baseMetodo.html', context={
        'proyectos': results,
        'boards': Board.objects.order_by('id'),
        'columns': Column.objects.order_by('id'),
        'cards': Card.objects.order_by('id'),
    })


def new_card(request):
    column_id = int(request.POST.get('column_id'))
    title = request.POST.get('title')
    assert title and column_id
    Card.objects.create(title=title, column_id=column_id)
    return redirect('/?q='+str(url))


def view_card(request, card_id, card_slug):
    return render(request, template_name='viewMetodo.html', context={
        'board': Board.objects.all(),
        'current_card': Card.objects.get(id=card_id),
    })


def drop(request):
    payload = json.loads(request.body)
    card_id = int(payload.get('card_id'))
    column_id = int(payload.get('column_id'))
    assert card_id and column_id
    card = Card.objects.get(id=card_id)
    card.column = Column.objects.get(id=column_id)
    card.save()
    return HttpResponse()


# class dashboard(request):
#   proyecto = Proyecto.objects.all()

#	return render(request,'dashboard.html')

# Create your views here.
# context['codigo']=Proyecto.codigo
'''class prueba(ListView):
	model=Proyecto
	template_name='dashboard.html'
	context_object_name='proyecto'
	def get_queryset(self):
		return Proyecto.objects.order_by('codigo')
	def get_context_data(self,**kwargs):
		context=super(prueba,self).get_context_data(**kwargs)
		return context'''

# Create your views here.
