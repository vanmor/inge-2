from django.urls import path,include
from .views import index,drop,new_card,view_card

app_name = 'back'
urlpatterns = [

    path('new-card/', new_card,name='newCard'),
    path('cards/<int:card_id>/<slug:card_slug>/',view_card,name='viewCard'),
    path('drop/',drop,name='dropCard'),
    path('',index,name='indexCard'),
]