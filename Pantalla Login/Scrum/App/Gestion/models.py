from django.db import models
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify
from django.utils.text import slugify

# Create your models here.

class Rol(models.Model):
    id_rol = models.CharField(max_length=12, primary_key=True)
    descripcion = models.CharField(max_length=30)

    def __str__(self):
        return self.id_rol + ',' + self.descripcion

    class Meta:
        verbose_name_plural = 'ROL'

    # el UsuarioRol el usuario puede tener muchos roles
# un rol puede tener muchos usuarios
# class UsuarioRol(models.Model):
#	usuario=models.ForeignKey(get_user_model(),on_delete=models.CASCADE)
#	codRol=models.ForeignKey(Rol,on_delete=models.CASCADE)
#	def __str__(self):
#		return str(self.usuario)+','+str(self.codRol)

class Usuario(models.Model):
    codigo = models.CharField(max_length=12, primary_key=True)
    #usuario = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='userRol')
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE, related_name='rolUser')

    def __str__(self):
        return str(self.usuario) + ',' + str(self.rol)

    class Meta:
        verbose_name_plural = 'USUARIO'

class Proyecto(models.Model):
    activo = 'A'
    inactivo = 'I'
    estados_proyecto = ((activo, 'Activo'), (inactivo, 'Inactivo'),)
    codigo_proyecto = models.CharField(max_length=12, primary_key=True,)
    nombre_proyecto = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=200, help_text='Detalles del proyecto')
    fechaInicio = models.DateField(max_length=30, verbose_name ='Fecha de Inicio')
    fechaEntrega = models.DateField(max_length=30, verbose_name ='Fecha de Entrega')
    estado = models.CharField(max_length=2, choices= estados_proyecto, default=activo)
    equipoDesarrollo = models.ManyToManyField(Usuario, through='Equipo',through_fields=('proyectoU', 'usuarioP'),)
    ProductOwner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = 'PROYECTO'

    def __str__(self):
        return self.codigo_proyecto + ',' + self.nombre_proyecto + ',' + self.estado

    '''Equipo es una clase intermedia entre User 
    y Proyecto para que se pueda usa el EquipoDesarrollo 
    ManyToManyField de proyecto'''

class Equipo(models.Model):
    codigo_equipo = models.CharField(max_length=12, primary_key=True)
    usuarioP = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    proyectoU = models.ForeignKey(Proyecto, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'EQUIPO'
        verbose_name_plural = 'EQUIPO'
    def __str__(self):
        return 'Usuario'


#class UserStory(models.Model):
    #alto = 'A'
    #medio = 'M'
    #bajo = 'B'
    #codigo_us = models.CharField(max_length=12, primary_key=True)
    #proyectoUS = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    #nombre = models.CharField(max_length=30)
    #descripcion = models.CharField(max_length=200)
    #responsable = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    #niveles = ((alto,'Alto')), (medio,'Medio'), (bajo,'Bajo')
    #nivel_de_dificultad = models.CharField(max_length=2, choices=niveles, default=alto)
    #por_hacer='PH'
    #haciendo='HA'
    #hecho='HE'
    #estados = ((por_hacer,'Por Hacer'),(haciendo,'Haciendo'),(hecho,'Hecho'),)
    #estado = models.CharField(max_length=2,choices=estados,default=por_hacer)
    #class Meta:
        #verbose_name = 'USER STORY'
        #verbose_name_plural = 'USER STORY'
    #def __str__(self):
        #return self.codigo_us + ',' + self.nombre + self.descripcio

class ProductBacklog(models.Model):
    #product_backlog = models.ForeignKey(UserStory, on_delete=models.CASCADE)
    class Meta:
        # verbose_name = 'USER STORY'
        verbose_name_plural = 'PRODUCT BACKLOG'

    def __str__(self):
        return 'PRODUCT BACKLOG'

class SprintBacklog(models.Model):
    por_hacer = 'PH'
    en_proceso = 'EP'
    finalizado = 'F'
    codigo_sprint = models.CharField(max_length=12, primary_key=True)
    estados = ((por_hacer, 'Por Hacer'), (en_proceso, 'En Proceso'), (finalizado, 'Finalizado'),)
    estado = models.CharField(max_length=2, choices=estados, default=por_hacer)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    #userstoryS = models.ForeignKey(UserStory, on_delete=models.CASCADE)
    class Meta:
        verbose_name_plural = 'SPRINT BACKLOG'
    def __str__(self):
        return self.codigo_sprint + ',' + self.estado

    from django.db import models
    from django.utils.text import slugify

class Board(models.Model):
        project = models.ForeignKey(Proyecto,on_delete=models.CASCADE,related_name='projects')
        name = models.CharField(max_length=255)

        def __str__(self):
            return self.name

class Column(models.Model):
        board = models.ForeignKey('Board',on_delete=models.CASCADE, related_name='columns')
        title = models.CharField(max_length=255)

        class Meta:
            ordering = ['id']

        def __str__(self):
            return '{} - {}'.format(self.board.name, self.title)

class Card(models.Model):
        proyectoUSC = models.ForeignKey(Proyecto, on_delete=models.CASCADE,null=True,blank=True)
        responsableC = models.ForeignKey(get_user_model(), on_delete=models.CASCADE,null=True,blank=True,related_name='owner')
        column = models.ForeignKey('Column', related_name='cards',on_delete=models.CASCADE)
        title = models.CharField(max_length=255)
        slug = models.SlugField(allow_unicode=True)
        class Meta:
            ordering = ['id']
            verbose_name_plural = 'SPRINT BACKLOG'

        def __str__(self):
            return '{} - {} - {}'.format(self.column.board.name, self.column.title, self.title)

        def save(self, *args, **kwargs):
            if not self.slug:
                self.slug = slugify(self.title, allow_unicode=True)
            return super().save(*args, **kwargs)

#el UsuarioRol el usuario puede tener muchos roles
#un rol puede tener muchos usuarios
#class UsuarioRol(models.Model):
#	usuario=models.ForeignKey(get_user_model(),on_delete=models.CASCADE)
#	codRol=models.ForeignKey(Rol,on_delete=models.CASCADE)
#	def __str__(self):
#		return str(self.usuario)+','+str(self.codRol)
