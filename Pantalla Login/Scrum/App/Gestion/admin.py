from django.contrib import admin
from App.Gestion.models import *
admin.site.register(Rol)
admin.site.register(Usuario)
admin.site.register(Equipo)
admin.site.register(Proyecto)
admin.site.register(Board)
admin.site.register(Card)
admin.site.register(Column)
#admin.site.register(UserStory)
admin.site.register(SprintBacklog)
admin.site.register(ProductBacklog)

# Register your models here.s
