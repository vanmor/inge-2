# Generated by Django 2.1.7 on 2019-03-27 14:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Rol',
            fields=[
                ('id_rol', models.CharField(max_length=12, primary_key=True, serialize=False)),
                ('descripcion', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('codigo', models.CharField(max_length=12, primary_key=True, serialize=False)),
                ('rol', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='rolUser', to='Gestion.Rol')),
            ],
        ),
    ]
